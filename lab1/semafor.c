#include <pthread.h>
#include <stdlib.h>
#include <sys/signal.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <semaphore.h>

typedef struct porukaSemafor_
{
    long mtype;
    int smjerAuti;
    int smjerPjesaci;

}porukaSemafor;

typedef struct porukaPjesak_
{
    long mtype;
    int id;

}porukaPjesak;

typedef struct tred
{
    int tid;
    int id;
    int n;
};

typedef struct shmstruct
{
    int id;
    int brojacPjesakSJ;
    int brojacPjesakIZ;
    int brojacAutoSJ;
    int brojacAutoIZ;
}shmstruct_;



pthread_mutex_t count_lock;
pthread_cond_t count_nonzero;

struct shmstruct * shmp;
int shmid;

#define SHM_KEY 0x1239
#define BILLION  1E9;

void posaljiDaKrenu(int smjer, struct shmstruct * shmp,int msqid)
{
    switch (smjer)
    {
    case 1:
    {
        int size = shmp->brojacPjesakSJ;
        for(int i=0;i<size;i++)
        {
            printf("SALJEM PJESAK SMJER 6\n");
            porukaPjesak pjesak1 = {.mtype = 6, .id = 1};
            if( msgsnd(msqid, (struct msgbuf* )&pjesak1, sizeof( struct porukaPjesak_ ) - sizeof(long), IPC_NOWAIT) == -1)
            {
                printf("Error2");
            }
        }
        shmp->brojacPjesakSJ -= size;
        break;
    }
    case 2:
    {
        int size = shmp->brojacPjesakIZ;
        for(int i=0;i<shmp->brojacPjesakIZ;i++)
        {
            printf("SALJEM PJESAK SmJER 7\n");
            porukaPjesak pjesak2 = {.mtype = 7, .id = 1};
            if( msgsnd(msqid, (struct msgbuf* )&pjesak2, sizeof( struct porukaPjesak_ ) - sizeof(long), IPC_NOWAIT) == -1)
            {
                printf("Error3");
            }
        }
        shmp->brojacPjesakIZ -= size;
        break;
    }
    case 3:
    {
        int size = shmp->brojacPjesakSJ;
        for(int i=0;i<size;i++)
        {
            printf("SALJEM AUTO SMJER 10\n");
            porukaPjesak pjesak1 = {.mtype = 10, .id = 1};
            if( msgsnd(msqid, (struct msgbuf* )&pjesak1, sizeof( struct porukaPjesak_ ) - sizeof(long), IPC_NOWAIT) == -1)
            {
                printf("Error2");
            }
        }
        shmp->brojacPjesakSJ -= size;
        break;
    }
    case 4:
    {
        int size = shmp->brojacPjesakIZ;
        for(int i=0;i<shmp->brojacPjesakIZ;i++)
        {
            printf("SALJEM AUTO SmJER 11\n");
            porukaPjesak pjesak2 = {.mtype = 11, .id = 1};
            if( msgsnd(msqid, (struct msgbuf* )&pjesak2, sizeof( struct porukaPjesak_ ) - sizeof(long), IPC_NOWAIT) == -1)
            {
                printf("Error3");
            }
        }
        shmp->brojacPjesakIZ -= size;
        break;
    }
    default:
        break;
    }

}

void BrisanjeShm()
{
    shmdt((struct shmstruct *)shmp);
    shmctl(shmid, IPC_RMID, NULL);
}

void PrekidnaRutina(int sig)
{
    BrisanjeShm();
    exit(0);
}

int main( int argc, char ** argv )
{
    srand(time(NULL));

    sigset(SIGINT, PrekidnaRutina);

    key_t key;
    key = getuid();
    int msqid;

    shmid = shmget(SHM_KEY, sizeof(struct shmstruct), 0644|IPC_CREAT);

    shmp = shmat(shmid, NULL, 0);

    if ( shmp->id == 0 )
    {
        shmp->id = 1;
        shmp->brojacPjesakSJ = 0;
        shmp->brojacPjesakIZ = 0;
        shmp->brojacAutoSJ = 0;
        shmp->brojacAutoIZ = 0;
    }

    //spajanje na red poruka ili stvaranje ako nije stvoren
    if((msqid = msgget(key, 0600 | IPC_CREAT)) == -1)
    {
        printf("error msgget");
        exit(1);
    }

    int pjesaciSmjer = 0;
    int autoSmjer = 0;
    printf("Uspjeso sve inicijalizirano\n");

    while(1)
    {
        posaljiDaKrenu(pjesaciSmjer, shmp, msqid);
        posaljiDaKrenu(autoSmjer, shmp, msqid);
        porukaSemafor porukaSem;

        if( msgrcv(msqid, (struct msgbuf* )&porukaSem, sizeof( struct porukaSemafor_ ) - sizeof(long), 5, IPC_NOWAIT) == -1 )
        {
            continue;
        }

        pjesaciSmjer = porukaSem.smjerPjesaci;
        autoSmjer = porukaSem.smjerAuti;
        printf("PROMJENA SMJERA PRIMLJENA %d %d\n", porukaSem.smjerAuti, porukaSem.smjerPjesaci);
    }
    return 0;
}