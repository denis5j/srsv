#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>

typedef struct porukaPjesak_
{
    long mtype;
    int trenutnaLokacija;
    int smjer;
    int prelazim;

}porukaPjesak;

#define BILLION  1E9;

typedef struct porukaSemafor_
{
    long mtype;
    int smjerAuti;
    int smjerPjesaci;

}porukaSemafor;

#define SHM_KEY 0x1239

typedef struct shmstruct
{
    int id;
    int brojacPjesakSJ;
    int brojacPjesakIZ;
    int brojacAutoSJ;
    int brojacAutoIZ;
}shmstruct_;


int shmid;
struct shmstruct * shmp;

void BrisanjeShm()
{
    shmdt((struct shmstruct *)shmp);
    shmctl(shmid, IPC_RMID, NULL);
}

void PrekidnaRutina(int sig)
{
    BrisanjeShm();
    exit(0);
}

int main( int argc, char ** argv )
{
    srand(time(NULL));
    shmid = shmget(SHM_KEY, sizeof(struct shmstruct), 0644|IPC_CREAT);

    if (shmid == -1) {
    perror("Shared memory");
    return 1;
    }
    shmp = shmat(shmid, NULL, 0);

    key_t key;
    key = getuid();

    int smjerPjesaci = 1;
    int smjerAuti = 1;
    int poslanoZaPjesake= 0;

    int msqid;
    //spajanje na red poruka ili stvaranje ako nije stvoren
    if((msqid = msgget(key, 0600 | IPC_CREAT)) == -1)
    {
        printf("error msgget");
        exit(1);
    }

    sleep(1);

    struct timespec requestStart, requestEnd;
    clock_gettime(CLOCK_REALTIME, &requestStart);
    while(1)
    {
        clock_gettime(CLOCK_REALTIME, &requestEnd);
        double accum = ( requestEnd.tv_sec - requestStart.tv_sec ) + ( requestEnd.tv_nsec - requestStart.tv_nsec ) / BILLION;
        if
        (
            accum > 30 ||
            (   accum > 15                &&
                shmp->brojacPjesakSJ == 0 &&
                shmp->brojacPjesakIZ == 0 &&
                shmp->brojacAutoSJ   == 0 &&
                shmp->brojacAutoIZ   == 0
            )
        )
        {
            printf("SLANJE PROMJENE SMJERA za AUTE\n %f",accum);
            clock_gettime(CLOCK_REALTIME, &requestStart);
            clock_gettime(CLOCK_REALTIME, &requestEnd);
            smjerAuti = ( smjerAuti + 1 ) % 2;
            smjerPjesaci = ( smjerPjesaci + 1 ) % 2;
            porukaSemafor porukaSemafor = {.mtype = 5, .smjerAuti = smjerAuti+1, .smjerPjesaci = smjerPjesaci+1};
            if( msgsnd(msqid, (struct msgbuf* )&porukaSemafor, sizeof( struct porukaSemafor_ ) - sizeof(long), 0) == -1)
            {
                printf("Error");
            }
            printf("SALJEM %d %d\n", smjerAuti, smjerPjesaci);
            poslanoZaPjesake = 0;
        }
        if(
            (accum > 20 ||
            (   accum > 5                 &&
                shmp->brojacPjesakSJ == 0 &&
                shmp->brojacPjesakIZ == 0 &&
                shmp->brojacAutoSJ   == 0 &&
                shmp->brojacAutoIZ   == 0
            )
            ) && !poslanoZaPjesake
        )
        {
            printf("SLANJE PROMJENE SMJERA za PJESAKE\n");
            porukaSemafor porukaSemafor = {.mtype = 5, .smjerAuti = smjerAuti, .smjerPjesaci = 0};
            if( msgsnd(msqid, (struct msgbuf* )&porukaSemafor, sizeof( struct porukaSemafor_ ) - sizeof(long), 0) == -1)
            {
                printf("Error");
            }
            printf("SALJEM %d %d\n", smjerAuti, smjerPjesaci);
            poslanoZaPjesake = 1;
        }
        porukaPjesak pjesackaPoruka;
        if( msgrcv(msqid, (struct msgbuf* )&pjesackaPoruka, sizeof( struct porukaPjesak_ ) - sizeof(long), 4, IPC_NOWAIT) == -1 )
        {
            continue;
        }
        else {
            if(pjesackaPoruka.smjer == 1)
            {
                shmp->brojacPjesakSJ += 1;
            }else {
                shmp->brojacPjesakIZ += 1;
            }
            printf("%d %d\n",shmp->brojacPjesakSJ,shmp->brojacPjesakIZ);
            printf("PRIMJEN PJESAK S SMJEROM %d\n", pjesackaPoruka.smjer);
        }
        if( msgrcv(msqid, (struct msgbuf* )&pjesackaPoruka, sizeof( struct porukaPjesak_ ) - sizeof(long), 9, IPC_NOWAIT) == -1 )
        {
        }
        else {
            if(pjesackaPoruka.smjer == 1)
            {
                shmp->brojacAutoSJ += 1;
            }else {
                shmp->brojacAutoIZ += 1;
            }
            printf("%d %d\n",shmp->brojacPjesakSJ,shmp->brojacPjesakIZ);
            printf("PRIMJEN AUTO S SMJEROM %d\n", pjesackaPoruka.smjer);
        }
        if( msgrcv(msqid, (struct msgbuf* )&pjesackaPoruka, sizeof( struct porukaPjesak_ ) - sizeof(long), 14, IPC_NOWAIT) == -1 )
        {
        }
        else {
            //prima pjesaka koji je presao
        }

    }

    int trajanje = rand() % 2 + 1;

    return 0;
}