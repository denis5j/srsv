#include <pthread.h>
#include <stdlib.h>
#include <sys/signal.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <semaphore.h>

typedef struct porukaPjesak_
{
    long mtype;
    int trenutnaLokacija;
    int smjer;
    int prelazim;

}porukaPjesak;


typedef struct porukaPjesakSem_
{
    long mtype;
    int id;

}porukaPjesakSem;


struct shmstruct * shmp;
int shmid;

#define SHM_KEY 0x1239


int main( int argc, char ** argv )
{
    srand(time(NULL));


    key_t key;
    key = getuid();
    int msqid;

    //spajanje na red poruka ili stvaranje ako nije stvoren
    if((msqid = msgget(key, 0600 | IPC_CREAT)) == -1)
    {
        printf("error msgget");
        exit(1);
    }

    printf("Uspjeso sve inicijalizirano\n");

    //posaljiPjesacimaDaKrenu(autoSmjer, shmp, msqid);
    int smjer = rand()%2 + 1;
    int trenutnaLokacija = rand()%4 +1 ;
    porukaPjesak porukaPjesak1 = {.mtype = 4, .trenutnaLokacija = trenutnaLokacija, .smjer = smjer, .prelazim = 0};
    if( msgsnd(msqid, (struct msgbuf* )&porukaPjesak1, sizeof( struct porukaPjesak_ ) - sizeof(long), 0) == -1)
    {
        printf("Error");
    }
    printf("POSLANO UPRU");
    porukaPjesakSem porukaSem;
    if( msgrcv(msqid, (struct msgbuf* )&porukaSem, sizeof( struct porukaPjesakSem_ ) - sizeof(long), 5 + smjer, 0) == -1 )
    {
        printf("Error1");
    }
    sleep(10);
    porukaPjesak porukaPjesak2 = {.mtype = 14, .trenutnaLokacija = trenutnaLokacija, .smjer = smjer, .prelazim = 0};
    if( msgsnd(msqid, (struct msgbuf* )&porukaPjesak2, sizeof( struct porukaPjesak_ ) - sizeof(long), 0) == -1)
    {
        printf("Error");
    }
    return 0;
}