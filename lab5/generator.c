#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>

typedef struct posao_
{
    long mtype;
    int id;
    int trajanje;

}posao;

typedef struct shmstruct
{
    pthread_mutex_t mux;
    int unqId;
}shmstruct_;

#define SHM_KEY 0x1234

static int uniqueId = 1;

int main( int argc, char ** argv )
{
    srand(time(NULL));
    int J = atoi( argv[ 1 ] );
    int K = atoi( argv[ 2 ] );

    int shmid;
    struct shmstruct * shmp;
    shmid = shmget(SHM_KEY, sizeof(struct shmstruct), 0644|IPC_CREAT);

    if (shmid == -1) {
    perror("Shared memory");
    return 1;
    }
    shmp = shmat(shmid, NULL, 0);
    if ( shmp->unqId == 0 )
    {
        pthread_mutex_t mux1;
        shmp->unqId = 0;
        shmp->mux = mux1;
    }

    key_t key;
    key = getuid();

    int msqid;

    //spajanje na red poruka ili stvaranje ako nije stvoren
    if((msqid = msgget(key, 0600 | IPC_CREAT)) == -1)
    {
        printf("error msgget");
        exit(1);
    }

    for( int i=0; i < J; i++ )
    {
        pthread_mutex_lock(&shmp->mux);
        sleep(1);
        int id = shmp->unqId++;
        pthread_mutex_unlock(&shmp->mux);
        int trajanje = rand() % K + 1;
        posao poslic = {.mtype = 2, .id = id, .trajanje = trajanje};
        if( msgsnd(msqid, (struct msgbuf* )&poslic, sizeof( struct posao_ ) - sizeof(long), 0) == -1)
        {
            printf("Error");
        }
        printf("SALJEM %d %d\n", id, trajanje);
    }
    return 0;
}