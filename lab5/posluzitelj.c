#include <pthread.h>
#include <stdlib.h>
#include <sys/signal.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <semaphore.h>

typedef struct posao_
{
    long mtype;
    int id;
    int trajanje;

}posao;

typedef struct tred
{
    int tid;
    int id;
    int n;
};

typedef struct shmstruct
{
    pthread_mutex_t mux;
    int unqId;
}shmstruct_;

struct shmstruct * shmp;
int shmid;

#define SHM_KEY 0x1234
#define BILLION  1E9;

static unsigned long long forLoopIterations = 400000000U;

void ThreadFunkcijaObrada(void* arg)
{
    struct tred * treed = arg;
    struct timespec requestStart, requestEnd;
    clock_gettime(CLOCK_REALTIME, &requestStart);
    asm volatile ("":::"memory");
    for( int j = 0; j < treed->n; j++ )
    {
        int podatak = rand()%999;
        printf("R%d: id:%d obrada podataka: %d %d/%d\n",treed->tid, treed->id, podatak, j+1, treed->n );
        for( int i = 0; i < forLoopIterations; i++ )
        {
        }
    }
    clock_gettime(CLOCK_REALTIME, &requestEnd);

    // Calculate time it took
    double accum = ( requestEnd.tv_sec - requestStart.tv_sec )
    + ( requestEnd.tv_nsec - requestStart.tv_nsec )
    / BILLION;
}

void KalibracijaForPetlje()
{
    struct timespec requestStart, requestEnd;
    for( int i=0; i < 4; i++ )
    {
        clock_gettime(CLOCK_REALTIME, &requestStart);
        asm volatile ("":::"memory");
        for( int i=0; i < forLoopIterations; i++ )
        {
        }
        clock_gettime(CLOCK_REALTIME, &requestEnd);

        // Calculate time it took
        double accum = ( requestEnd.tv_sec - requestStart.tv_sec )
        + ( requestEnd.tv_nsec - requestStart.tv_nsec )
        / BILLION;
        printf("Kalibriranje: ForLoopIter: %llu Vrijeme: %f \n", forLoopIterations, accum);
        forLoopIterations/=accum;
    }
}

void BrisanjeShm()
{
    shmdt((struct shmstruct *)shmp);
    shmctl(shmid, IPC_RMID, NULL);
}

void PrekidnaRutina(int sig)
{
    BrisanjeShm();
    exit(0);
}

int main( int argc, char ** argv )
{
    srand(time(NULL));
    int N = atoi( argv[ 1 ] );
    pthread_t thread_id[ N ];

    sigset(SIGINT, PrekidnaRutina);

    struct posao_ poslic;

    KalibracijaForPetlje();

    key_t key;
    key = getuid();
    int msqid;

    struct posao_ *poslovi = calloc(10, sizeof(*poslovi));
    shmid = shmget(SHM_KEY, sizeof(struct shmstruct), 0644|IPC_CREAT);

    shmp = shmat(shmid, NULL, 0);

    //spajanje na red poruka ili stvaranje ako nije stvoren
    if((msqid = msgget(key, 0600 | IPC_CREAT)) == -1)
    {
        printf("error msgget");
        exit(1);
    }
    int pokazivacNaPrvi = 0;
    int pokazivacNaZadnji = 0;
    while(1)
    {
        struct timespec requestStart, requestEnd;
        int zauzeto = pokazivacNaZadnji - pokazivacNaPrvi;
        clock_gettime(CLOCK_REALTIME, &requestStart);
        double accum = 0;
        while(N - zauzeto > 0 && !( accum > 30 && zauzeto > 0) )
        {
            clock_gettime(CLOCK_REALTIME, &requestEnd);
            accum = ( requestEnd.tv_sec - requestStart.tv_sec )
            + ( requestEnd.tv_nsec - requestStart.tv_nsec )
            / BILLION;

            if( msgrcv(msqid, (struct msgbuf* )&poslic, sizeof( struct posao_ ) - sizeof(long), 2, IPC_NOWAIT) == -1 )
            {
                continue;
            }
            printf("PRIMAM %d %d\n", poslic.id, poslic.trajanje);
            poslovi[zauzeto] = poslic;
            zauzeto++;
        }

        for ( int i = 0; i < zauzeto; i++ )
        {
            struct tred * treeed = calloc(1, sizeof(treeed));
            treeed->tid = i;
            treeed->id = poslovi[i].id;
            treeed->n = poslovi[i].trajanje;
            if(pthread_create( &thread_id[ i ], NULL, ThreadFunkcijaObrada, treeed) != 0 )
            {
                printf("ERROR TREAD");
                exit(0);
            }
        }
        for ( int i = 0; i < zauzeto; i++ )
        {
            pthread_join( thread_id[i], NULL );
        }
    }
    return 0;
}