#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <ostream>

#include "putnik.cpp"

#define MAXINLIFT 8

enum class Door : std::uint8_t
{
    Open,
    Closed
};

enum class Direction : std::uint8_t
{
    Down,
    Up,
    None
};

class Lift
{
public:
    Lift()
    {
        currentFloor = Floor::FirstAndHalf;
        wantedFloor = Floor::Second;
        wantedFloors.push_back( Floor::FirstAndHalf );
        direction = Direction::None;
        door = Door::Closed;
        outOfLift = std::vector< std::vector < Passenger > >(10);
    }
    friend std::ostream& operator<<(std::ostream& os, const Lift& lift)
    {
        os << "[";
        for( auto passenger : lift.inLift )
        {
            os << passenger.name;
        }
        for( int i = 0;i < MAXINLIFT - lift.inLift.size(); i++ )
        {
            os << " ";
        }
        os << "]";
        return os;
    }
    void nextStep(std::vector<Passenger> & passengers)
    {
        auto currFloor = static_cast< int >( currentFloor );
        if ( inLift.empty() && currentFloor == wantedFloor )
        {
            if ( !passengers.empty() )
            {
                wantedFloor = passengers.front().currentFloor;
                direction = wantedFloor == currentFloor ? Direction::None : ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor )  < 0 ? Direction::Down : Direction::Up );
            }
        }
        if ( currFloor % 2 == 0)
        {
            if
            (
                (
                    !std::any_of
                    (
                        passengers.cbegin(),
                        passengers.cend(),
                        [ & ]( Passenger i )
                        {
                            auto poravnavanje = ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor ) );
                            auto poravnavanje2 = ( static_cast< int >( i.wantedFloor ) - static_cast< int >( i.currentFloor ) );
                            return ( i.currentFloor == currentFloor ) && (poravnavanje * poravnavanje2 >=0 );
                        }
                    )                       &&
                wantedFloor != currentFloor &&
                door == Door::Closed
                )
                ||
                (
                    inLift.size() >= MAXINLIFT  &&
                    wantedFloor != currentFloor &&
                    door == Door::Closed        &&
                    !std::any_of(inLift.begin(), inLift.end(), [&](Passenger i){ return currentFloor == i.wantedFloor; } )
                )
            )
            {
                auto poravnavanje = ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor ) );
                if ( poravnavanje == 0 )
                {
                    return;
                }
                currentFloor = static_cast< Floor >( currFloor +  poravnavanje / abs( poravnavanje ) );
                return;
            }
            switch ( step )
            {
                case 0:
                {
                    if ( inLift.empty() && passengers.empty() )
                    {
                        return;
                    }
                    door = Door::Open;
                    break;
                }
                case 1:
                {
                    for (auto it = inLift.begin(); it != inLift.end(); it++)
                    {
                        if ( it->wantedFloor == currentFloor )
                        {
                            outOfLift[ static_cast< int >( currentFloor ) ].push_back( *it );
                            inLift.erase( it-- );
                        }
                    }
                    for (auto it = passengers.begin(); it != passengers.end(); it++)
                    {
                        if ( it->currentFloor == currentFloor && inLift.size() != MAXINLIFT )
                        {
                            if ( !inLift.empty() )
                            {
                                auto poravnavanje = ( static_cast< int >( inLift.front().wantedFloor ) - static_cast< int >( currentFloor ) );
                                auto poravnavanje2 = ( static_cast< int >( it->wantedFloor ) - static_cast< int >( currentFloor ) );
                                if ( poravnavanje * poravnavanje2 > 0)
                                {
                                    inLift.push_back( *it  );
                                    passengers.erase( it-- );
                                    wantedFloor = std::max( inLift.back().wantedFloor, wantedFloor );
                                }
                            }
                            else
                            {
                                inLift.push_back( *it  );
                                passengers.erase( it-- );
                                wantedFloor = inLift.front().wantedFloor;
                            }
                        }
                    }
                    if( inLift.empty() && passengers.empty() )
                    {
                        wantedFloor = currentFloor;
                    }
                    break;
                }
                case 2:
                {
                    door = Door::Closed;
                    if ( !inLift.empty() )
                    {
                       wantedFloor = inLift.front().wantedFloor;
                    }
                    auto poravnavanje = ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor ) );
                    if ( poravnavanje > 0 )
                    {
                        wantedFloor = std::max_element(inLift.begin(), inLift.end(), [](Passenger a, Passenger b ) { return a.wantedFloor < b.wantedFloor; } )->wantedFloor;
                    }
                    else if ( poravnavanje < 0)
                    {
                        wantedFloor = std::min_element(inLift.begin(), inLift.end(), [](Passenger a, Passenger b ) { return a.wantedFloor < b.wantedFloor; } )->wantedFloor;
                    }
                    break;
                }
                case 3:
                {
                    auto poravnavanje = ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor ) );
                    if ( poravnavanje == 0)
                    {
                        break;
                    }
                    currentFloor = static_cast< Floor >( currFloor +  poravnavanje / abs( poravnavanje ) );
                    break;
                }
            }
            direction = wantedFloor == currentFloor ? Direction::None : ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor )  < 0 ? Direction::Down : Direction::Up );
            step++;
            step%=3;
            return;
        }
        auto poravnavanje = ( static_cast< int >( wantedFloor ) - static_cast< int >( currentFloor ) );
        if ( poravnavanje == 0)
        {
            return;
        }
        currentFloor = static_cast< Floor >( currFloor +  poravnavanje / abs( poravnavanje ) );
    }
    void printExitedOnFloor ( int i )
    {
        if (outOfLift.size() > i +1 )
        {
            for( auto passenger : outOfLift[i] )
            {
                std::cout << passenger.name;
            }
        }
    }
    std::vector< Passenger > inLift;
    std::vector< std::vector< Passenger > > outOfLift;
    Floor currentFloor;
    Floor wantedFloor;
    std::vector< Floor > wantedFloors;
    Direction direction;
    Door door;
    int step = 0;
};