#include <cstdint>
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <vector>

#define MAXINFRONTOFLIFT 9

static std::vector < int > counter(4,0);

enum class passengerStatus : std::uint8_t
{
    OnEtrence,
    InLift,
    Outside
};

enum class Floor : std::uint8_t
{
    First,
    FirstAndHalf,
    Second,
    SecondAndHalf,
    Third,
    ThirdAndHalf,
    Fourth
};

class Passenger
{
public:
    Passenger() : isInLift( false )
    {
        name = rand() % 58 + 65;

        if
        (
            counter [ 0 ] == MAXINFRONTOFLIFT &&
            counter [ 1 ] == MAXINFRONTOFLIFT &&
            counter [ 2 ] == MAXINFRONTOFLIFT &&
            counter [ 3 ] == MAXINFRONTOFLIFT
        )
        {
            delete this;
            return;
        }

        int currFloor;
        do
        {
            currentFloor = static_cast< Floor >( ( rand() % 4 ) * 2 );
            currFloor = static_cast< int >( currentFloor );
        }
        while ( counter[ currFloor / 2 ] >= MAXINFRONTOFLIFT );
        counter[ currFloor / 2 ]++;

        do
        {
            wantedFloor = static_cast< Floor >( ( rand() % 4 ) * 2 );
        }
        while( wantedFloor == currentFloor );
    }
    ~Passenger()
    {
        int currFloor = static_cast< int >( currentFloor );
        counter[ currFloor / 2 ]--;
    }
    friend std::ostream& operator<<(std::ostream& os, const Passenger& passenger)
    {
        os << passenger.name;
        return os;
    }
    char name;
    bool isInLift;
    Floor wantedFloor;
    Floor currentFloor;

};