#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <vector>

#include "lift.cpp"

#define MAXPUTNIKA 10

void printLift(Lift & lift, Floor floor)
{
    if(lift.currentFloor == floor)
    {
        std::cout << lift;
    }
    else
    {
        std::cout << "          ";
    }
}

void printFunction( std::vector< Passenger > passengers, Lift lift)
{
    std::cout << "\t\t" << "Lift 1" << std::endl;
    std::cout << "Smjer/Vrata:" << "\t";
    std::cout << ( lift.direction == Direction::Down ? "D " : lift.direction == Direction::Up ? "G " : "N " );
    std::cout << ( lift.door == Door::Open ? "O" : "Z" ) << std::endl;
    std::cout << "Stajanja:=====  " << "****" << "  === **** == Izasli" << std::endl;
    for(int i=6;i>=0;i--)
    {
        if( i%2 == 0 )
        {
            std::cout << i / 2 + 1 <<":";
            int count = 0;
            for(auto passenger : passengers)
            {
                if( static_cast< Floor >( i ) == passenger.currentFloor )
                {
                    std::cout << passenger;
                    count++;
                }
            }
            for(int i=0;i< MAXPUTNIKA - count ;i++)
            {
                std::cout << " ";
            }
            std::cout << "|";
            printLift(lift, (Floor)i);
            std::cout << "|";
            lift.printExitedOnFloor( i );
            std::cout << std::endl;
        }
        else
        {
            if(i)
            {
                std::cout << "  ==========|";
                printLift(lift, (Floor)i);
                std::cout << "|";
                std::cout << std::endl;
            }
        }
    }
    std::cout << "=================================" << std::endl;
    std::cout << "Putnici: ";
    for( auto passenger : passengers )
    {
        std::cout << passenger;
    }
    std::cout << std::endl;
    std::cout << "     od: ";
    for( auto passenger : passengers )
    {
        std::cout << static_cast< int >( passenger.currentFloor ) + 1;
    }
    std::cout << std::endl;
    std::cout << "     do: ";
    for( auto passenger : passengers )
    {
        std::cout << static_cast< int >( passenger.wantedFloor ) + 1;
    }
    std::cout << std::endl;
}

int main(int argc, char ** argv )
{
    std::vector<Passenger> passengers;
    Lift lift;

    int N = atoi( argv[ 1 ] );
    int M = atoi( argv[ 2 ] );
    while( true )
    {
        for ( int i = 0; i < rand() % N ; i++ )
        {
            passengers.push_back({});
        }
        for ( int i = 0; i <  M ; i++ )
        {
            printFunction(passengers, lift);
            lift.nextStep(passengers);
            usleep(1000000);
        }
    }
    return 0;
}